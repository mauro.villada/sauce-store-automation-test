# language: es

Característica: Compra de productos
  Como cliente logueado en la tienda virtual de Sauce
  necesito agregar productos al carro de compras
  para efectuar la compra

  Escenario: Completar compra
    Dado que el cliente se encuentra logueado en la tienda de Sauce
    Cuando el cliente complete el checkout de los productos en el carro de compras
    Entonces el cliente será notificado de la orden completa