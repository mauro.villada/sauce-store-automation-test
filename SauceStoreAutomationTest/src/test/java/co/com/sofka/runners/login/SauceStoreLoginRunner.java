package co.com.sofka.runners.login;



import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/sauce-ligin/sauce_login.feature"},
        glue = {"co.com.sofka.definitions.login"},
        tags = ""
)
public class SauceStoreLoginRunner {
}
