package co.com.sofka.runners.checkout;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/checkout/product_sauce_checkout.feature"},
        glue = {"co.com.sofka.definitions.checkout"},
        tags = ""
)
public class SauceProductCheckoutRunner {
}
