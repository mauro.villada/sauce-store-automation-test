package co.com.sofka.utils;

public enum DataLogin {
    STANDAR_USER_NAME("standard_user"),
    STANDAR_PASSWORD("secret_sauce");

    private final String value;

    public String getValue() {
        return value;
    }

    DataLogin(String value) {
        this.value = value;
    }
}
