package co.com.sofka.utils;

import java.time.Duration;

public enum SecondsTime {
    ZERO_SECONDS(Duration.ofSeconds(0)),
    ONE_SECONDS(Duration.ofSeconds(1)),
    TWO_SECONDS(Duration.ofSeconds(2)),
    THREE_SECONDS(Duration.ofSeconds(3)),
    FOUR_SECONDS(Duration.ofSeconds(4)),
    FIVE_SECONDS(Duration.ofSeconds(5)),
    SIX_SECONDS(Duration.ofSeconds(6)),
    SEVEN_SECONDS(Duration.ofSeconds(7)),
    EIGHT_SECONDS(Duration.ofSeconds(8)),
    NINE_SECONDS(Duration.ofSeconds(9)),
    TEN_SECONDS(Duration.ofSeconds(10)),
    TWENTY_SECONDS(Duration.ofSeconds(20)),
    THIRTY_SECONDS(Duration.ofSeconds(30)),
    SIXTY_SECONDS(Duration.ofSeconds(60));


    private final Duration value;

    SecondsTime(Duration value) {
        this.value = value;
    }

    public Duration getValue() {
        return value;
    }
}
