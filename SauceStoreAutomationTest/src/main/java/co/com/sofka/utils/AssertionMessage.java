package co.com.sofka.utils;

public enum AssertionMessage {
    SUCCESSFULL_LOGIN("PRODUCTS"),
    LOGIN_ERROR("Epic sadface: Username and password do not match any user in this service"),
    CHECKOUT_COMPLETE("CHECKOUT: COMPLETE!");

    private final String value;

    public String getValue() {
        return value;
    }

    AssertionMessage(String value) {
        this.value = value;
    }
}
