package co.com.sofka.pages.common;

import co.com.sofka.common.CommonActionOnPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonSaucePage extends CommonActionOnPage {
    public CommonSaucePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @CacheLookup
    @FindBy(id = "user-name")
    protected WebElement userName;

    @CacheLookup
    @FindBy(id = "password")
    protected WebElement password;

    @CacheLookup
    @FindBy(id = "login-button")
    protected WebElement loginButton;

    @CacheLookup
    @FindBy(css = "#login_button_container > div > form > div.error-message-container.error > h3")
    protected WebElement loginError;

    @CacheLookup
    @FindBy(css = "#header_container > div.header_secondary_container > span")
    protected WebElement sauceStore;

    protected By productContainer = By.cssSelector("button[class=\"btn btn_primary btn_small btn_inventory\"]");

    @CacheLookup
    @FindBy(id = "shopping_cart_container")
    protected WebElement shoppingCart;

    @CacheLookup
    @FindBy(id = "checkout")
    protected WebElement cartCheckout;

    @CacheLookup
    @FindBy(id = "first-name")
    protected WebElement firstName;

    @CacheLookup
    @FindBy(id = "last-name")
    protected WebElement lastName;

    @CacheLookup
    @FindBy (id = "postal-code")
    protected WebElement postalCode;

    @CacheLookup
    @FindBy(id = "continue")
    protected WebElement checkoutContinue;

    @CacheLookup
    @FindBy(id = "finish")
    protected WebElement checkoutFinish;

    @CacheLookup
    @FindBy(css = "#header_container > div.header_secondary_container > span")
    protected WebElement checkoutComplete;


}
