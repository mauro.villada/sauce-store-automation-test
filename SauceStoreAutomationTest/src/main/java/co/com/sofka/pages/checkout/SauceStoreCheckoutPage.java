package co.com.sofka.pages.checkout;

import co.com.sofka.pages.common.CommonSaucePage;
import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;

public class SauceStoreCheckoutPage extends CommonSaucePage {
    private static final Faker faker = new Faker();
    public SauceStoreCheckoutPage(WebDriver driver) {
        super(driver);
    }

    public void fillOrderInformation(){
        scrollOn(firstName);
        clearBox(firstName);
        typeOn(firstName, faker.name().firstName());

        scrollOn(lastName);
        clearBox(lastName);
        typeOn(lastName, faker.name().lastName());

        scrollOn(postalCode);
        clearBox(postalCode);
        typeOn(postalCode, faker.address().zipCode());
    }

    public void continueCheckout(){
        scrollOn(checkoutContinue);
        clickOn(checkoutContinue);
    }

    public void confirmeCheckout(){
        explicitWaitWeb(checkoutFinish);
        clickOn(checkoutFinish);
    }

    public String isCheckoutComplete(){
        explicitWaitWeb(checkoutComplete);
        return getText(checkoutComplete);
    }

}
