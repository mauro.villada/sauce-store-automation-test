package co.com.sofka.pages.login;

import co.com.sofka.pages.common.CommonSaucePage;
import org.openqa.selenium.WebDriver;

public class SauceStoreLoginPage extends CommonSaucePage {
    public SauceStoreLoginPage(WebDriver driver) {
        super(driver);
    }

    public void fillLoginSuccessful(String user, String passwordUser){
        scrollOn(userName);
        clearBox(userName);
        typeOn(userName, user);

        scrollOn(password);
        clearBox(password);
        typeOn(password, passwordUser);

        scrollOn(loginButton);
        clickOn(loginButton);

    }
    public String isLoginOk(){
        explicitWaitWeb(sauceStore);
        return getText(sauceStore);

    }
    public String isLoginFail(){
        explicitWaitWeb(loginError);
        return getText(loginError);

    }


}
