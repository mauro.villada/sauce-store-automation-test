package co.com.sofka.pages.cart;

import co.com.sofka.pages.common.CommonSaucePage;
import org.openqa.selenium.WebDriver;

public class SauceStoreCartPage extends CommonSaucePage {
    public SauceStoreCartPage(WebDriver driver) {
        super(driver);
    }

    public void goToCheckout(){
        explicitWaitWeb(cartCheckout);
        scrollOn(cartCheckout);
        clickOn(cartCheckout);
    }
}
