package co.com.sofka.pages.store;

import co.com.sofka.pages.common.CommonSaucePage;
import org.openqa.selenium.WebDriver;

public class SauceStorePage extends CommonSaucePage {
    public SauceStorePage(WebDriver driver) {
        super(driver);
    }

    public void addProductToCart(){
        productSelect(productContainer);
        scrollOn(shoppingCart);
        clickOn(shoppingCart);
    }
}
