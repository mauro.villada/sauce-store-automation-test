package co.com.sofka.definitions.checkout;


import co.com.sofka.pages.cart.SauceStoreCartPage;
import co.com.sofka.pages.checkout.SauceStoreCheckoutPage;
import co.com.sofka.pages.login.SauceStoreLoginPage;
import co.com.sofka.pages.store.SauceStorePage;
import co.com.sofka.setup.SetupWebUI;
import co.com.sofka.utils.AssertionMessage;
import co.com.sofka.utils.DataLogin;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class SauceStoreCheckoutDefinition extends SetupWebUI {

    private static final Logger LOGGER = Logger.getLogger(SauceStoreCheckoutDefinition.class);
    SauceStoreCheckoutPage sauceStoreCheckoutPage;

    @Dado("que el cliente se encuentra logueado en la tienda de Sauce")
    public void queElClienteSeEncuentraLogueadoEnLaTiendaDeSauce() {
        try{
            setUpLog4j2();
            setUpWebDriver();

            SauceStoreLoginPage sauceStoreLoginPage = new SauceStoreLoginPage(driver);
            sauceStoreLoginPage
                    .fillLoginSuccessful(DataLogin.STANDAR_USER_NAME.getValue(),DataLogin.STANDAR_PASSWORD.getValue());
        } catch (Exception e){
            quiteDriver();
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Cuando("el cliente complete el checkout de los productos en el carro de compras")
    public void elClienteCompleteElCheckoutDeLosProductosEnElCarroDeCompras() {
        SauceStorePage sauceStorePage = new SauceStorePage(driver);
        SauceStoreCartPage sauceStoreCartPage = new SauceStoreCartPage(driver);
        sauceStoreCheckoutPage = new SauceStoreCheckoutPage(driver);
        try{
            sauceStorePage.addProductToCart();
            sauceStoreCartPage.goToCheckout();
            sauceStoreCheckoutPage.fillOrderInformation();
            sauceStoreCheckoutPage.continueCheckout();
            sauceStoreCheckoutPage.confirmeCheckout();
        } catch (Exception e){
            quiteDriver();
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Entonces("el cliente será notificado de la orden completa")
    public void elClienteSeraNotificadoDeLaOrdenCompleta() {
        assertThat(AssertionMessage.CHECKOUT_COMPLETE.getValue(), equalTo(sauceStoreCheckoutPage.isCheckoutComplete()));
        quiteDriver();
    }
}
