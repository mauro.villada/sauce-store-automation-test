package co.com.sofka.definitions.login;

import co.com.sofka.pages.login.SauceStoreLoginPage;
import co.com.sofka.setup.SetupWebUI;
import co.com.sofka.utils.AssertionMessage;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import co.com.sofka.utils.DataLogin;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;


public class SauceStoreLoginDefinition extends SetupWebUI {
    private static final Logger LOGGER = Logger.getLogger(SauceStoreLoginDefinition.class);
    SauceStoreLoginPage sauceStoreLoginPage;
    private static final Faker faker = new Faker();

    @Dado("que el cliente se encuentra en la página principal de Sauce")
    public void queElClienteSeEncuentraEnLaPaginaPrincipalDeSauce() {
        try{
            setUpLog4j2();
            setUpWebDriver();
        } catch (Exception e){
            quiteDriver();
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("el cliente presione login con usuario y contraseña registrados")
    public void elClientePresioneLoginConUsuarioYContrasenaRegistrados() {
        try{
            sauceStoreLoginPage = new SauceStoreLoginPage(driver);
            sauceStoreLoginPage
                    .fillLoginSuccessful(DataLogin.STANDAR_USER_NAME.getValue(),DataLogin.STANDAR_PASSWORD.getValue());
        } catch (Exception e){
            quiteDriver();
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el cliente podrá iniciar sesión de forma satisfactoria")
    public void elClientePodraIniciarSesionDeFormaSatisfactoria() {
        assertThat(AssertionMessage.SUCCESSFULL_LOGIN.getValue(), equalTo(sauceStoreLoginPage.isLoginOk()));
        quiteDriver();
    }

    @Cuando("el cliente presione login con datos de usuario no registrado")
    public void elClientePresioneLoginConDatosDeUsuarioNoRegistrado() {
        try{
            sauceStoreLoginPage = new SauceStoreLoginPage(driver);
            sauceStoreLoginPage.fillLoginSuccessful(faker.internet().emailAddress(), faker.internet().password());
        } catch (Exception e){
            quiteDriver();
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Entonces("el cliente verá un mensaje de error indicnado que el usuario no ha sido registrado")
    public void elClienteVeraUnMensajeDeErrorIndicnadoQueElUsuarioNoHaSidoRegistrado() {
        assertThat(AssertionMessage.LOGIN_ERROR.getValue(), equalTo(sauceStoreLoginPage.isLoginFail()));
        quiteDriver();
    }
}
