package co.com.sofka.common;

import co.com.sofka.utils.SecondsTime;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class CommonActionOnPage {
    private static final Logger LOGGER = Logger.getLogger(CommonActionOnPage.class);

    private WebDriver driver;

    public CommonActionOnPage(WebDriver driver) {
        try {
            if(driver == null)
                LOGGER.warn("El Webdriver es nulo.");

            this.driver = driver;

        }catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
        }
    }

    /**
     * Funcionalidades genéricas
     */

    protected void clearBox(WebElement webElement){
        webElement.clear();
    }

    protected void clickOn(WebElement webElement){
        webElement.click();
    }

    protected void typeOn(WebElement webElement, CharSequence... keysToSend){
        webElement.sendKeys(keysToSend);
    }

    protected void scrollOn(WebElement webElement){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView();", webElement);
    }
    protected String getText(WebElement webElement){
        return webElement.getText();
    }
    protected void explicitWaitWeb(WebElement webElement){
        new WebDriverWait(driver, SecondsTime.THIRTY_SECONDS.getValue()).until(ExpectedConditions.visibilityOf(webElement));
    }

    public void productSelect(By locator){
        List<WebElement> containerElements = driver.findElements(locator) ;
        Random r = new Random();
        int randomValue = r.nextInt(containerElements.size());
        containerElements.get(randomValue).click();
    }


}

