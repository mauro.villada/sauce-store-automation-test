# language: es
  Característica: Inicio de sesión para usuarios
    Como cliente registrado en el la tienda virtual de Sauce
    necesito iniciar sesión
    para efectuar compras en línea

  Antecedentes:
    Dado que el cliente se encuentra en la página principal de Sauce

  Escenario: Inicio de sesión exitoso
    Cuando el cliente presione login con usuario y contraseña registrados
    Entonces el cliente podrá iniciar sesión de forma satisfactoria


  Escenario: Inicio de sesión fallido
    Cuando el cliente presione login con datos de usuario no registrado
    Entonces el cliente verá un mensaje de error indicnado que el usuario no ha sido registrado

